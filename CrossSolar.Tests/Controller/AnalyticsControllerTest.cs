﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossSolar.Controllers;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Newtonsoft.Json;
using Xunit;

namespace CrossSolar.Tests.Controller
{
    [Trait("Analytic Data is Requested with a valid panel id", "")]
    public class ValidAnalyticsDataRequested
    {
        [Fact(DisplayName = "Day results is returned")]
        public async Task DayResultsDataIsReturned()
        {
            var options = new DbContextOptionsBuilder<CrossSolarDbContext>()
                .UseInMemoryDatabase("AnalyticsDataIsReturned").Options;
            var testData = new OneHourElectricity[]
            {
                new OneHourElectricity()
                {
                    KiloWatt = 10,
                    DateTime = new DateTime(2018, 8, 11, 5, 0, 0),
                    PanelId = "0000000000000001",
                    Id = 1
                },
                new OneHourElectricity()
                {
                    KiloWatt = 30,
                    DateTime = new DateTime(2018, 8, 11, 7, 0, 0),
                    PanelId = "0000000000000001",
                    Id = 2
                },
                new OneHourElectricity()
                {
                    KiloWatt = 20,
                    DateTime = new DateTime(2018, 8, 11, 8, 0, 0),
                    PanelId = "0000000000000001",
                    Id = 3
                },
                new OneHourElectricity()
                {
                    KiloWatt = 20,
                    DateTime = new DateTime(2018, 8, 15, 8, 0, 0),
                    PanelId = "0000000000000001",
                    Id = 4
                }
            };

            using (var dbContext = new CrossSolarDbContext(options))
            {
                var analyticsRepository = new AnalyticsRepository(dbContext);
                var panelRepositroy = new PanelRepository(dbContext);

                await panelRepositroy.InsertAsync(new Panel()
                {
                    Serial = "0000000000000001",
                    Id = 1,
                    Brand = "Test",
                    Latitude = 1,
                    Longitude = 1
                });

                foreach (var oneHourElectricity in testData)
                {
                    await analyticsRepository.InsertAsync(oneHourElectricity);
                }

            }

            using (var dbContext = new CrossSolarDbContext(options))
            {
                var panelRepositroy = new PanelRepository(dbContext);
                var analyticsRepository = new AnalyticsRepository(dbContext);
                var controller = new AnalyticsController(analyticsRepository, panelRepositroy);
                var result = await controller.DayResults("0000000000000001");
                Assert.IsAssignableFrom<OkObjectResult>(result);
                Assert.IsAssignableFrom<List<OneDayElectricityModel>>(((OkObjectResult)result).Value);
                var resultData = (List<OneDayElectricityModel>)((OkObjectResult)result).Value;
                Assert.Equal(2, resultData.Count);
                Assert.Equal(resultData[0].DateTime, new DateTime(2018, 8, 11));
                Assert.Equal(10, resultData[0].Minimum);
                Assert.Equal(30, resultData[0].Maximum);
                Assert.Equal(20, resultData[0].Average);
                Assert.Equal(resultData[1].DateTime, new DateTime(2018, 8, 15));
                Assert.Equal(20, resultData[1].Minimum);
                Assert.Equal(20, resultData[1].Maximum);
                Assert.Equal(20, resultData[1].Average);
            }
        }


        [Fact(DisplayName = "Data is returned")]
        public async Task DataIsReturned()
        {
            var options = new DbContextOptionsBuilder<CrossSolarDbContext>()
                .UseInMemoryDatabase("DataIsReturned").Options;
            var testData = new OneHourElectricity[]
            {
                new OneHourElectricity()
                {
                    KiloWatt = 10,
                    DateTime = new DateTime(2018, 8, 11, 5, 0, 0),
                    PanelId = "0000000000000001",
                    Id = 1
                },
                new OneHourElectricity()
                {
                    KiloWatt = 30,
                    DateTime = new DateTime(2018, 8, 11, 7, 0, 0),
                    PanelId = "0000000000000001",
                    Id = 2
                },
                new OneHourElectricity()
                {
                    KiloWatt = 20,
                    DateTime = new DateTime(2018, 8, 11, 8, 0, 0),
                    PanelId = "0000000000000001",
                    Id = 3
                },
                new OneHourElectricity()
                {
                    KiloWatt = 20,
                    DateTime = new DateTime(2018, 8, 15, 8, 0, 0),
                    PanelId = "0000000000000002",
                    Id = 4
                }
            };

            using (var dbContext = new CrossSolarDbContext(options))
            {
                var analyticsRepository = new AnalyticsRepository(dbContext);
                var panelRepositroy = new PanelRepository(dbContext);

                await panelRepositroy.InsertAsync(new Panel()
                {
                    Serial = "0000000000000001",
                    Id = 1,
                    Brand = "Test",
                    Latitude = 1,
                    Longitude = 1
                });

                foreach (var oneHourElectricity in testData)
                {
                    await analyticsRepository.InsertAsync(oneHourElectricity);
                }

            }

            using (var dbContext = new CrossSolarDbContext(options))
            {
                var panelRepositroy = new PanelRepository(dbContext);
                var analyticsRepository = new AnalyticsRepository(dbContext);
                var controller = new AnalyticsController(analyticsRepository, panelRepositroy);
                var result = await controller.Get("0000000000000001");
                Assert.IsAssignableFrom<OkObjectResult>(result);
                Assert.IsAssignableFrom<OneHourElectricityListModel>(((OkObjectResult)result).Value);
                var resultData = (OneHourElectricityListModel)((OkObjectResult)result).Value;
                Assert.Equal(3, resultData.OneHourElectricitys.Count());
            }
        }
    }

    [Trait("Valid analytic data submitted","")]
    public class ValidAnalyticsDataSubmitted
    {
        [Fact(DisplayName = "Data saved to DB. Created response returned.")]
        public async Task DataSavedToDb()
        {
            var options = new DbContextOptionsBuilder<CrossSolarDbContext>()
                .UseInMemoryDatabase("DataSavedToDb").Options;
            using (var dbContext = new CrossSolarDbContext(options))
            {
                var panelRepositroy = new PanelRepository(dbContext);
                await panelRepositroy.InsertAsync(new Panel()
                {
                    Serial = "0000000000000001",
                    Id = 1,
                    Brand = "Test",
                    Latitude = 1,
                    Longitude = 1
                });
                var analyticsRepository = new AnalyticsRepository(dbContext);
                var controller = new AnalyticsController(analyticsRepository, panelRepositroy);
                var response = await controller.Post("0000000000000001", new OneHourElectricityModel()
                {
                    DateTime = new DateTime(2018, 8, 15),
                    KiloWatt = 10,
                    Id = 1
                });
                Assert.IsAssignableFrom<CreatedResult>(response);
                Assert.IsAssignableFrom<OneHourElectricityModel>(((CreatedResult)response).Value);
            }
        }
    }

}