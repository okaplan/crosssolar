﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CrossSolar.Controllers
{
    [Route("panel")]
    public class AnalyticsController : Controller
    {
        private readonly IAnalyticsRepository _analyticsRepository;

        private readonly IPanelRepository _panelRepository;

        public AnalyticsController(IAnalyticsRepository analyticsRepository, IPanelRepository panelRepository)
        {
            _analyticsRepository = analyticsRepository;
            _panelRepository = panelRepository;
        }

        // GET panel/XXXX1111YYYY2222/analytics
        [HttpGet("{banelId}/[controller]")]
        public async Task<IActionResult> Get([FromRoute] string panelId)
        {
            if (string.IsNullOrWhiteSpace(panelId) || panelId.Length != 16)
            {
                return StatusCode(422, "Panel Id should contain 16 characters.");
            }

            if (!await _panelRepository.Query()
                .AnyAsync(x => x.Serial.Equals(panelId, StringComparison.CurrentCultureIgnoreCase)))
            {
                return NotFound($"PanelId {panelId} not found");
            }

            var analytics = await _analyticsRepository.Query()
                .Where(x => x.PanelId.Equals(panelId, StringComparison.CurrentCultureIgnoreCase))
                .ToListAsync();

            var result = new OneHourElectricityListModel
            {
                OneHourElectricitys = analytics.Select(c => new OneHourElectricityModel
                {
                    Id = c.Id,
                    KiloWatt = c.KiloWatt,
                    DateTime = c.DateTime
                })
            };

            return Ok(result);
        }

        // GET panel/XXXX1111YYYY2222/analytics/day
        [HttpGet("{panelId}/[controller]/day")]
        public async Task<IActionResult> DayResults([FromRoute] string panelId)
        {
            if (string.IsNullOrWhiteSpace(panelId) || panelId.Length != 16)
            {
                return StatusCode(422, "Panel Id should contain 16 characters.");
            }

            if (!await _panelRepository.Query().AnyAsync(x => x.Serial.Equals(panelId, StringComparison.CurrentCultureIgnoreCase)))
            {
                return NotFound($"PanelId {panelId} not found");
            }
            var result = await _analyticsRepository.Query()
                .Where(w => w.PanelId == panelId && w.DateTime < DateTime.Today)
                .GroupBy(g => g.DateTime.Date).Select(s => new OneDayElectricityModel()
                {
                    DateTime = s.Key,
                    Average = s.Average(k => k.KiloWatt),
                    Maximum = s.Max(k => k.KiloWatt),
                    Minimum = s.Min(k => k.KiloWatt),
                    Sum = s.Sum(k => k.KiloWatt)
                })
                .OrderBy(o => o.DateTime)
                .ToListAsync();

            return Ok(result);
        }

        // POST panel/XXXX1111YYYY2222/analytics
        [HttpPost("{panelId}/[controller]")]
        public async Task<IActionResult> Post([FromRoute] string panelId, [FromBody] OneHourElectricityModel value)
        {
            if (string.IsNullOrWhiteSpace(panelId) || panelId.Length != 16)
            {
                return StatusCode(422, "Panel Id should contain 16 characters.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var oneHourElectricityContent = new OneHourElectricity
            {
                PanelId = panelId,
                KiloWatt = value.KiloWatt,
                DateTime = DateTime.UtcNow
            };

            await _analyticsRepository.InsertAsync(oneHourElectricityContent);

            var result = new OneHourElectricityModel
            {
                Id = oneHourElectricityContent.Id,
                KiloWatt = oneHourElectricityContent.KiloWatt,
                DateTime = oneHourElectricityContent.DateTime
            };

            return Created($"panel/{panelId}/analytics/{result.Id}", result);
        }
    }
}